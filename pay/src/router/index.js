import Vue from 'vue'
import Router from 'vue-router'
import Article from '@/components/article/article.vue'
import problemAll from '@/base/problem/problemAll'


Vue.use(Router)

export default new Router({
  mode: 'history',
  data(){
    return {
      newsId:1
    }
  },
  routes: [
    {
      path: '/',
      name: 'Article',
      component: Article
    },
    {
      path: '/problemAll',
      name: 'problemAll',
      component: problemAll
    }
  ]
})
